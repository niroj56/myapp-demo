<h1 align="center">
  <br>
  <br>
  My Application Demo - an mobile app
  <br>
   <a href="https://travis-ci.org/nirooj56/onsale"><img src="https://travis-ci.org/nirooj56/onsale.svg?branch=master" alt="Travis"></a>
  <a href="https://github.com/nirooj56/onsale/releases"><img src="https://img.shields.io/github/release/nirooj56/onsale.svg" alt="Release"></a>
  <a href="https://github.com/nirooj56/onsale/blob/master/Licence"><img src="https://img.shields.io/github/license/nirooj56/onsale.svg" alt="Release"></a>
  <br>
</h1>
